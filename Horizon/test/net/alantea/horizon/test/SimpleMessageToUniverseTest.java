package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;

@TestMethodOrder(MethodName.class)
public class SimpleMessageToUniverseTest
{
   private static TheListener listener1 = new TheListener("One");
   private static TheListener listener2 = new TheListener("Two");
   
   @Test
   public void T00_testSetMode()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
   }
   
   @Test
   public void T01_testSetFirstListener()
   {
      Messenger.registerAllMessages(listener1);
   }

   @Test
   public void T02_testSendGlobalMessageToOneListener()
   {
      Messenger.sendMessage(this, null, TheListener.FIRSTID, 666, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertTrue(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener1.getContent(), 666);
      Messenger.sendMessage(this, null, TheListener.SECONDID, Math.PI, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertTrue(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SECONDID);
      Assertions.assertEquals(listener1.getContent(), Math.PI);
   }

   @Test
   public void T03_testSendSpecificMessage()
   {
      Messenger.sendMessage(this, null, TheListener.SPECIFICID, "specific", false);
      Assertions.assertTrue(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SPECIFICID);
      Assertions.assertEquals(listener1.getContent(), "specific");
      
      Messenger.sendMessage(this, null, TheListener.SPECIFICID, 123L, false);
      Assertions.assertTrue(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SPECIFICID);
      Assertions.assertEquals(listener1.getContent(), 123L);
   }

   @Test
   public void T04_testSendSpecialMessage()
   {
      Messenger.sendMessage(this, null, TheListener.SPECIALID, listener1, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertTrue(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SPECIALID);
      Assertions.assertEquals(listener1.getContent(), listener1);
   }

   @Test
   public void T05_testSendUnknownMessage()
   {
      Messenger.sendMessage(this, null, TheListener.ANOTHERID, true, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener1.getContent(), true);
   }

   @Test
   public void T06_testSetSecondListener()
   {
      Messenger.registerAllMessages(listener2);
   }

   @Test
   public void T07_testSendMessage2()
   {
      Messenger.sendMessage(this, null, TheListener.FIRSTID, 666, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertEquals(listener1.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener1.getContent(), 666);
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertEquals(listener2.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener2.getContent(), 666);
      Messenger.sendMessage(this, null, TheListener.SECONDID, Math.PI, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertEquals(listener1.getId(), TheListener.SECONDID);
      Assertions.assertEquals(listener1.getContent(), Math.PI);
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertEquals(listener2.getId(), TheListener.SECONDID);
      Assertions.assertEquals(listener2.getContent(), Math.PI);
   }
}