package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.test.model.TheListener;
import net.alantea.horizon.test.model.TheListener2;

@TestMethodOrder(MethodName.class)
public class ListenMessagesTest
{
   private static final String CONTEXT1 = "Context1";
   
   private static TheListener2 listener = new TheListener2("One");
   private static String theSource = "Dummy one";

   @Test
   public void T00_testSendNormalMessage()
   {
      // send message in context 1
      Messenger.sendSynchronousMessage(CONTEXT1, theSource, listener, TheListener.FIRSTID, 666, false);
      Assertions.assertFalse(listener.isSpecific());
      Assertions.assertFalse(listener.isSpecial());
      Assertions.assertTrue(listener.isBackup());
      Assertions.assertNull(listener.getGotIdentifier());
      Assertions.assertEquals(listener.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener.getContent(), 666);
   }

   @Test
   public void T01_testSendAnotherMessage()
   {
      // send message in context 1
      Messenger.sendSynchronousMessage(CONTEXT1, theSource, listener, TheListener.ANOTHERID, 666, false);
      Assertions.assertFalse(listener.isSpecific());
      Assertions.assertFalse(listener.isSpecial());
      Assertions.assertFalse(listener.isBackup());
      Assertions.assertEquals(listener.getGotIdentifier(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener.getId(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener.getContent(), 666);
   }
}
