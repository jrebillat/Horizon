package net.alantea.horizon.test;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;


@Suite
@SelectClasses({ 
   SimpleTest.class,
   SimpleMessageToUniverseTest.class,
   SimpleMessageExchangeTest.class,
   SimpleMessageToUniverseTestConcurrent.class,
   SimpleMessageToUniverseTestThreaded.class,
   SimpleMessageToUniverseTestHyperthreaded.class,
   HorizonListenerTest.class,
   HorizonSubscriptionTest.class,
   ContextsTest.class,
   SynchronousTest.class,
   ListenMessagesTest.class,
   PropertiesTest.class,
   SimpleMessageControlerTest.class
   })
public class AllTests
{

}
