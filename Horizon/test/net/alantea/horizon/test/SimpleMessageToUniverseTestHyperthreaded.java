package net.alantea.horizon.test;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;

@TestMethodOrder(MethodName.class)
public class SimpleMessageToUniverseTestHyperthreaded
{
   private TheListener listener1 = new TheListener("One");
   private TheListener listener2 = new TheListener("Two");
   
   @Test
   public void T00_testSetMode()
   {
      Messenger.setMode(Mode.HYPERTHREADED);
      Messenger.setInterval(-200);
      Messenger.setInterval(200);
   }
   
   @Test
   public void T01_testSetFirstListener()
   {
      Messenger.registerAllMessages(listener1);
   }
   
   @Test
   public void T02_testSendGlobalMessageToOneListener()
   {
      Messenger.sendMessage(this, null, TheListener.FIRSTID, 666, false);
      Messenger.sendMessage(this, null, TheListener.SECONDID, Math.PI, false);
   }
   
   @Test
   public void T03_testSendSpecificMessage()
   {
      Messenger.sendMessage(this, null, TheListener.SPECIFICID, "specific", false);
      
      Messenger.sendMessage(this, null, TheListener.SPECIFICID, 123L, false);
   }
   
   @Test
   public void T04_testSendSpecialMessage()
   {
      Messenger.sendMessage(this, null, TheListener.SPECIALID, listener1, false);
   }
   
   @Test
   public void T05_testSendUnknownMessage()
   {
      Messenger.sendMessage(this, null, TheListener.ANOTHERID, true, false);
   }
   
   @Test
   public void T06_testSetSecondListener()
   {
      Messenger.registerAllMessages(listener2);
   }
   
   @Test
   public void T07_testSendMessage2()
   {
      Messenger.sendMessage(this, null, TheListener.FIRSTID, 666, false);
      Messenger.sendMessage(this, null, TheListener.SECONDID, Math.PI, false);
   }
}