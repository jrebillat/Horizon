package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheControler;
import net.alantea.horizon.test.model.TheListener;

@TestMethodOrder(MethodName.class)
public class SimpleMessageControlerTest
{
   @Test
   public void T00_testSendGlobalMessageToControler()
   {
      TheControler.reinit();
      Messenger.setMode(Mode.SYNCHRONOUS);
      Assertions.assertFalse(TheControler.isCalled());
      Assertions.assertFalse(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
      
      Messenger.sendMessage(this, null, TheListener.FIRSTID, 666, false);
      Assertions.assertFalse(TheControler.isCalled());      
      Assertions.assertFalse(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
      
      Messenger.sendMessage(this, null, "TheControler::call", "You", false);
      Assertions.assertTrue(TheControler.isCalled());
      Assertions.assertFalse(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
      
      Messenger.sendMessage(this, null, "TheControler::call", "Me", false);
      Assertions.assertTrue(TheControler.isCalled());
      Assertions.assertTrue(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
      
      Messenger.sendMessage(this, null, "TheControler::call", "Me", false);
      Assertions.assertTrue(TheControler.isCalled());
      Assertions.assertTrue(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
      
      Messenger.sendMessage(this, null, TheListener.FIRSTID, 666, false);
      Assertions.assertTrue(TheControler.isCalled());
      Assertions.assertTrue(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
      
      Messenger.sendMessage(this, null, "TheControler::call", 3.14, false);
      Assertions.assertFalse(TheControler.isCalled());
      Assertions.assertFalse(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 3.14);
      
      Messenger.sendMessage(this, null, "TheControler::call", "You", false);
      Assertions.assertTrue(TheControler.isCalled());
      Assertions.assertFalse(TheControler.isMe());
      Assertions.assertTrue(TheControler.getWhat() == 0.0);
   }
}