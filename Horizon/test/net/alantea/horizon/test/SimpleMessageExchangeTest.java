package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Message;
import net.alantea.horizon.message.MessageSource;
import net.alantea.horizon.message.MessageSubscriber;
import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;

@TestMethodOrder(MethodName.class)
public class SimpleMessageExchangeTest implements MessageSource, MessageSubscriber
{
   private TheListener listener1 = new TheListener("One");
   private TheListener listener2 = new TheListener("Two");
   
   @Test
   public void testSetMode()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
   }
   
   @Test
   public void testSetFirstListener()
   {
      this.addHorizonListener(listener1);
   }
   
   public void T00_testSendMessageToOneListener()
   {
      sendMessage(listener2, TheListener.FIRSTID, Math.PI);
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertTrue(listener2.isBackup());
      Assertions.assertEquals(listener2.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener2.getContent(), Math.PI);

      Message message = new Message(this, listener2, TheListener.SPECIFICID, "Test", false);
      sendMessage(message);
      Assertions.assertTrue(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertFalse(listener2.isBackup());
      Assertions.assertEquals(listener2.getId(), TheListener.SPECIFICID);
      Assertions.assertEquals(listener2.getContent(), "Test");
   }

   @Test
   public void T01_testConfidentialMessageToOneListener()
   {
      sendConfidentialMessage(listener2, TheListener.FIRSTID, Math.PI);
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertTrue(listener2.isBackup());
      Assertions.assertEquals(listener2.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener2.getContent(), Math.PI);

      Message message = new Message(this, listener2, TheListener.SPECIFICID, "Test", false);
      sendMessage(message);
      Assertions.assertTrue(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertFalse(listener2.isBackup());
      Assertions.assertEquals(listener2.getId(), TheListener.SPECIFICID);
      Assertions.assertEquals(listener2.getContent(), "Test");
   }

   @Test
   public void T02_testSendMessageToAllListeners()
   {
      this.addHorizonListener(listener1);
      
      sendMessage(TheListener.SECONDID, 666);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertTrue(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SECONDID);
      Assertions.assertEquals(listener1.getContent(), 666);
      
      sendMessage(TheListener.ANOTHERID);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.ANOTHERID);
   }

   @Test
   public void T03_testRemoveListener()
   {
      this.removeHorizonListener(listener1);
   }

   @Test
   public void T04_testSubscribe()
   {
      Messenger.addSubscription(TheListener.ANOTHERID, this);
   }

   @Test
   public void T05_testUnsubscribe()
   {
      Messenger.removeSubscription(TheListener.ANOTHERID, this);
   }

   @Override
   public void onMessage(Message message)
   {
      
   }
}