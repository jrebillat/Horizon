package net.alantea.horizon.test;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Message;
import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;

@TestMethodOrder(MethodName.class)
public class HorizonSubscriptionTest
{
   private static TheListener listener1 = new TheListener("One");
   private static TheListener listener2 = new TheListener("Two");
   private static String theSource = "Dummy one";
   
   @Test
   public void T00_testSetMode()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
   }
   
   public void T01_testUnsubscribeEmpty()
   {
      Messenger.removeSubscription(TheListener.SPECIFICID, listener2);
   }
   
   public void T02_testSubscribeToNull()
   {
      Messenger.addSubscription(null, listener1);
   }
   
   public void T03_testSubscribeNullContext()
   {
      Messenger.addSubscription(null, "FakeIDentifier", listener1);
   }
   
   public void T04_testResubscribe()
   {
      Messenger.addSubscription(null, "FakeIDentifier", listener1);
   }
   
   public void T05_testSubscribeNull()
   {
      Messenger.addSubscription(TheListener.SPECIFICID, null);
   }
   
   public void T06_testRegisterNull()
   {
      Messenger.register(null);
   }
   
   public void T07_testRegisterContextNull()
   {
      Messenger.register(null, null);
   }
   
   public void T08_testRegister()
   {
      Messenger.register(listener1);
   }
   
   public void T09_testUnregisterDefault()
   {
      Messenger.unregister(listener1);
   }
   
   public void T10_testUnregisterDefaultNull()
   {
      Messenger.unregister(null);
   }
   
   public void T11_testRegisterContext()
   {
      Messenger.register("Context1", listener1);
      Messenger.register("Context2", listener1);
   }
   
   public void T12_testUnregisterContextIdentifier()
   {
      Messenger.unregister("Context2", listener1);
   }
   
   public void T13_testUnregisterNullContext()
   {
      Messenger.unregister(null, listener1);
   }
   
   public void T14_testUnregisterNullListenerContext()
   {
      Messenger.unregister("Context2", null);
   }
   
   public void T15_testUnregisterBadListenerContext()
   {
      Messenger.unregister("Context", "Anything");
   }
   
   public void T16_testUnregisterNotAListenerContext()
   {
      Messenger.register("Context3", listener1);
      Messenger.unregister("Context3", "Anything");
   }
   
   public void T17_testNullMessage()
   {
      Messenger.sendMessage(null);
   }
   
   public void T18_testUnregister()
   {
      Messenger.addSubscription(TheListener.SPECIFICID, listener2);
      Messenger.removeSubscription(TheListener.SPECIFICID, listener2);
      Messenger.removeSubscription(null, listener2);
      Messenger.removeSubscription(TheListener.SPECIFICID, null);
      Messenger.removeSubscription(null, TheListener.SPECIFICID, listener2);
   }
   
   public void T19_testAddHorizonListener()
   {
      Messenger.addListener(theSource, listener2);
      // re-add
      Messenger.addListener(theSource, listener2);
   }
   
   public void T20_testRemoveHorizonListener()
   {
      Messenger.removeListener(theSource, listener2);
      Messenger.removeListener(theSource, listener1);
      Messenger.removeListener(listener1, theSource);

   }
   
   public void T21_testSendMessage()
   {
      Message message = new Message(theSource, listener2, TheListener.SPECIFICID, "Test", false);
      message.setConfidential(true);
      Messenger.sendMessage(message);
      Messenger.sendMessage(new Message(theSource, listener2, TheListener.SPECIFICID, "Test", false));
      Messenger.sendConfidentialMessage(theSource, listener2, TheListener.SPECIFICID, "Test");
      Messenger.sendMessage(theSource, TheListener.SPECIFICID, "Test");
   }
   
   public void T22_testSendSynchronousMessage()
   {
      Messenger.sendSynchronousMessage(theSource, listener2, TheListener.SPECIFICID, "Test", false);
      Messenger.sendSynchronousMessage(new Message(theSource, listener2, TheListener.SPECIFICID, "Test", true));
      Messenger.sendSynchronousMessage(theSource, TheListener.SPECIFICID, "Test");
   }
   
   public void T23_testRemoveAllHorizonListener()
   {
      Messenger.removeAllListeners(listener2);
      Messenger.removeAllListeners(this);
      Messenger.removeAllListeners(null);
   }
   
   public void T24_testUnregisterAllMessages()
   {
      Messenger.registerAllMessages(listener1);
      Messenger.registerAllMessages(listener1);
      Messenger.unregisterAllMessages(listener1);
      Messenger.unregisterAllMessages(this);
      Messenger.unregisterAllMessages(null);
   }
   
   public void T25_testNullRegistering()
   {
      Messenger.registerAllMessages(null);
   }
   
   public void T26_testRemoveUnregistered()
   {
      Messenger.removeSubscription("DontCare", null);
   }
   
   public void T27_testUnregisterSillyMessages()
   {
      Messenger.removeSubscription("NothingHere", "DontCare");
   }
   
}
