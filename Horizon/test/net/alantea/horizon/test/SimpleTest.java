package net.alantea.horizon.test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Message;
import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;

/**
 * The Class SimpleTest.
 */
@TestMethodOrder(MethodName.class)
public class SimpleTest
{
   public static final String SPECIFICID = "Specific";
   public static final String SPECIALID = "Special";
   public static final String ANOTHERID = "Another";
   public static final String UNKNOWNID = "Unknown";

   public static final String SPECIFICCONTEXT = "SpecificContext";
   
   /**
    * Prepare.
    */
   @Test
   public void T00_prepareTest()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
   }
   
   /**
    * Simplest test.
    */
   @Test
   public void T01_simplestTest()
   {
      Something something = new Something();
      Messenger.addCatchAllSubscription(something);
      Assertions.assertFalse(something.yesOne);
      Assertions.assertFalse(something.yesTwo);
      
      Messenger.sendMessage(null, UNKNOWNID);
      Assertions.assertTrue(something.yesOne);
      Assertions.assertFalse(something.yesTwo);
      
      Messenger.sendMessage(null, SPECIFICID);
      Assertions.assertFalse(something.yesOne);
      Assertions.assertTrue(something.yesTwo);
   }
   
   /**
    * Very simple test.
    */
   @Test
   public void T02_verySimpleTest()
   {
      Something something = new Something();
      Messenger.addSubscription(UNKNOWNID, something);
      Messenger.addSubscription(SPECIFICID, something);
      Assertions.assertFalse(something.yesOne);
      Assertions.assertFalse(something.yesTwo);
      
      Messenger.sendMessage(null, UNKNOWNID);
      Assertions.assertTrue(something.yesOne);
      Assertions.assertFalse(something.yesTwo);
      
      Messenger.sendMessage(null, SPECIFICID);
      Assertions.assertFalse(something.yesOne);
      Assertions.assertTrue(something.yesTwo);
   }
   
   /**
    * Simple test.
    */
   @Test
   public void T03_simpleTest()
   {
      Something something1 = new Something();
      Something something2 = new Something();
      Messenger.addCatchAllSubscription(something1);
      Messenger.addSubscription(SPECIFICCONTEXT, SPECIFICID, something2);
      Assertions.assertFalse(something1.yesOne);
      Assertions.assertFalse(something1.yesTwo);
      Assertions.assertFalse(something2.yesOne);
      Assertions.assertFalse(something2.yesTwo);
      
      Messenger.sendMessage(null, UNKNOWNID);
      Assertions.assertTrue(something1.yesOne);
      Assertions.assertFalse(something1.yesTwo);
      Assertions.assertFalse(something2.yesOne);
      Assertions.assertFalse(something2.yesTwo);
      
      something1.yesOne = false;
      Messenger.sendMessage(SPECIFICCONTEXT, null, SPECIFICID, "");
      Assertions.assertFalse(something1.yesOne);
      Assertions.assertTrue(something1.yesTwo);
      Assertions.assertFalse(something2.yesOne);
      Assertions.assertTrue(something2.yesTwo);
   }
   
   private static class Something
   {
      boolean yesOne = false;
      boolean yesTwo = false;
      
      @SuppressWarnings("unused")
      public void onMessage(Message message)
      {
         yesOne = true;
      }
      
      @SuppressWarnings("unused")
      public void onSpecificMessage(Message message)
      {
         yesOne = false;
         yesTwo = true;
      }
   }
}
