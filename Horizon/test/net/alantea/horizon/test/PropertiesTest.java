package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;
import net.alantea.liteprops.StringProperty;

@TestMethodOrder(MethodName.class)
public class PropertiesTest
{
   private static final String CONTEXT2 = "Context2";
   
   private static TheListener listener1 = new TheListener("One");
   private static TheListener listener2 = new TheListener("Two");
   
   private static StringProperty property1 = new StringProperty();
   private static StringProperty property2 = new StringProperty();
   
   @Test
   public void T00_testSetInitial()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
      Messenger.register(listener1);
      Messenger.register(CONTEXT2, listener2);
   }

   @Test
   public void T01_testSetMonitoring()
   {
      Messenger.monitorProperty(property1, TheListener.SPECIALID);
   }

   @Test
   public void T02_testSetContextValue()
   {
      property2.set("Hello !!!");
      Messenger.monitorProperty(CONTEXT2, property2, TheListener.ANOTHERID, null);
      property2.set("Hello World");
      // verify listener2
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertFalse(listener2.isBackup());
      Assertions.assertEquals( TheListener.ANOTHERID, listener2.getId());
      Assertions.assertEquals(listener2.getContent(), property2.get());
   }

   @Test
   public void T03_testSetValue()
   {
      property1.set("Bingo");
      // verify listener1
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertTrue(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SPECIALID);
      Assertions.assertEquals(listener1.getContent(), property1.get());

      // verify listener2
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertFalse(listener2.isBackup());
      Assertions.assertEquals(listener2.getId(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener2.getContent(), property2.get());
   }
}
