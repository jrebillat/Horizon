package net.alantea.horizon.test;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.test.model.TheListener;

@TestMethodOrder(MethodName.class)
public class SynchronousTest
{
   private static final String CONTEXT1 = "Context1";
   private static final String CONTEXT2 = "Context2";
   
   private TheListener listener1 = new TheListener("One");
   private TheListener listener2 = new TheListener("Two");
   

   @Test()
   public void T00_testSendSynchronousMessage()
   {
      // send message in context 1
      Messenger.sendSynchronousMessage(CONTEXT1, this, listener1, TheListener.FIRSTID, 666, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertTrue(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener1.getContent(), 666);
   }

   @Test()
   public void T01_testNullMessage()
   {
      Messenger.sendSynchronousMessage(null);
   }

   @Test()
   public void T02_testDefaultSynchronousMessage()
   {
      Messenger.register(CONTEXT2, listener2);
      Messenger.sendSynchronousMessage(CONTEXT2, this, TheListener.ANOTHERID, 666);
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertFalse(listener2.isBackup());
      Assertions.assertEquals(listener2.getId(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener2.getContent(), 666);
   }
}
