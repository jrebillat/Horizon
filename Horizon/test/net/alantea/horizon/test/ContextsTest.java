package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;

/**
 * The Class ContextsTest.
 */
@TestMethodOrder(MethodOrderer.MethodName.class)
public class ContextsTest
{
   
   /** The Constant CONTEXT1. */
   private static final String CONTEXT1 = "Context1";
   
   /** The Constant CONTEXT2. */
   private static final String CONTEXT2 = "Context2";
   
   /** The listener 1. */
   private static TheListener listener1 = new TheListener("One");
   
   /** The listener 2. */
   private static TheListener listener2 = new TheListener("Two");
   
   /** The source. */
   private static String theSource = "Dummy one";
   
   /**
    * Test set mode.
    */
   @Test
   @Order(1)
   public void T00_testSetMode()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
   }
   
   /**
    * Test send global message to one listener.
    */
   @Test
   @Order(2)
   public void T01_testSendGlobalMessageToOneListener()
   {
      Messenger.register(CONTEXT1, listener1);
      Messenger.register(CONTEXT2, listener2);
      
      // send message in context 1
      Messenger.sendMessage(CONTEXT1, theSource, null, TheListener.SPECIALID, 666, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertTrue(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SPECIALID);
      Assertions.assertEquals(listener1.getContent(), 666);

      // send message in context 2
      Messenger.sendMessage(CONTEXT2, theSource, TheListener.ANOTHERID, Math.PI);
      // Verify that listener1 has not been changed
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertTrue(listener1.isSpecial());
      Assertions.assertFalse(listener1.isBackup());
      Assertions.assertEquals(listener1.getId(), TheListener.SPECIALID);
      Assertions.assertEquals(listener1.getContent(), 666);
      
      // verify listener 2 has been updated
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertEquals(listener2.getId(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener2.getContent(), Math.PI);

      // re-send message in context 1
      Messenger.sendMessage(CONTEXT1, theSource, null, TheListener.SPECIALID, 666, false);
      // verify that listener 2 has not been changed
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertEquals(listener2.getId(), TheListener.ANOTHERID);
      Assertions.assertEquals(listener2.getContent(), Math.PI);

   }
}
