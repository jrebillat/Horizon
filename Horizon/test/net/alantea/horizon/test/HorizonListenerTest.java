package net.alantea.horizon.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.MethodName;

import net.alantea.horizon.message.Messenger;
import net.alantea.horizon.message.Mode;
import net.alantea.horizon.test.model.TheListener;

/**
 * The Class HorizonListenerTest.
 */
@TestMethodOrder(MethodName.class)
public class HorizonListenerTest
{
   
   /** The listener 1. */
   private static TheListener listener1 = new TheListener("One");
   
   /** The listener 2. */
   private static TheListener listener2 = new TheListener("Two");
   
   private static String theSource = "Dummy one";
   
   /**
    * T 00 test set mode.
    */
   @Test
   @Order(1)
   public void T00_testSetMode()
   {
      Messenger.setMode(Mode.SYNCHRONOUS);
   }
   
   /**
    * T 01 test send message in the void.
    */
   @Test
   @Order(2)
   public void T01_testSendMessageInTheVoid()
   {
      Messenger.sendMessage(theSource, null, TheListener.FIRSTID, 666, false);
   }
   
   /**
    * T 02 test set bad listener.
    */
   @Test
   @Order(3)
   public void T02_testSetBadListener()
   {
      Messenger.addListener(theSource, null);
   }
   
   /**
    * T 03 test set silly listener.
    */
   @Test
   @Order(4)
   public void T03_testSetSillyListener()
   {
      Messenger.addListener(null, null);
   }
   
   /**
    * T 04 test set listener to void.
    */
   @Test
   @Order(5)
   public void T04_testSetListenerToVoid()
   {
      Messenger.addListener(null, theSource);
   }
   
   /**
    * T 05 test set first listener.
    */
   @Test
   @Order(6)
   public void T05_testSetFirstListener()
   {
      Messenger.addListener(theSource, listener1);
   }
   
   /**
    * T 06 test send global message to one listener.
    */
   @Test
   @Order(7)
   public void T06_testSendGlobalMessageToOneListener()
   {
      Messenger.sendMessage(theSource, null, TheListener.FIRSTID, 666, false);
      System.out.println(listener1);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertEquals(listener1.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener1.getContent(), 666);
      Messenger.sendMessage(theSource, null, TheListener.SECONDID, Math.PI, false);
      System.out.println(listener1);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertEquals(listener1.getId(), TheListener.SECONDID);
      Assertions.assertEquals(listener1.getContent(), Math.PI);
   }
   
   /**
    * T 07 test reset first listener.
    */
   @Test
   @Order(8)
   public void T07_testResetFirstListener()
   {
      Messenger.addListener(theSource, listener1);
   }
   
   /**
    * T 08 test set second listener.
    */
   @Test
   @Order(9)
   public void T08_testSetSecondListener()
   {
      Messenger.addListener(theSource, listener2);
   }
   
   /**
    * T 09 test send message 2.
    */
   @Test
   @Order(10)
   public void T09_testSendMessage2()
   {
      Messenger.addListener(theSource, listener2);
      Messenger.sendMessage(theSource, null, TheListener.FIRSTID, 666, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertEquals(listener1.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener1.getContent(), 666);
      Assertions.assertFalse(listener2.isSpecific());
      Assertions.assertFalse(listener2.isSpecial());
      Assertions.assertEquals(listener2.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener2.getContent(), 666);
   }
   
   /**
    * T 10 test remove second listener.
    */
   @Test
   @Order(11)
   public void T10_testRemoveSecondListener()
   {
      Messenger.removeListener(theSource, listener2);
   }
   
   /**
    * T 11 test remove from nothing listener.
    */
   @Test
   @Order(12)
   public void T11_testRemoveFromNothingListener()
   {
      Messenger.removeListener(theSource, listener2);
   }
   
   /**
    * T 12 test remove from anything listener.
    */
   @Test
   @Order(13)
   public void T12testRemoveFromAnythingListener()
   {
      Messenger.removeListener("AnyString", listener2);
   }
   
   /**
    * T 13 test remove not A listener.
    */
   @Test
   @Order(14)
   public void T13_testRemoveNotAListener()
   {
      Messenger.removeListener("SomeString", listener1);
      Messenger.removeListener("SomeString", listener2);
      
      // Clean
      Messenger.removeListener("SomeString", listener1);
   }
   
   /**
    * T 14 test send global message to only one listener.
    */
   @Test
   @Order(15)
   public void T14_testSendGlobalMessageToOnlyOneListener()
   {
      Messenger.sendMessage(theSource, null, TheListener.FIRSTID, 128, false);
      Assertions.assertFalse(listener1.isSpecific());
      Assertions.assertFalse(listener1.isSpecial());
      Assertions.assertEquals(listener1.getId(), TheListener.FIRSTID);
      Assertions.assertEquals(listener1.getContent(), 128);
      Assertions.assertEquals(listener2.getContent(), 666);
   }
   
   /**
    * T 15 test remove all listeners.
    */
   @Test
   @Order(16)
   public void T15_testRemoveAllListeners()
   {
      Messenger.removeAllListeners(theSource);
   }
   
   /**
    * T 16 test remove silly listeners.
    */
   @Test
   @Order(17)
   public void T16_testRemoveSillyListeners()
   {
      Messenger.removeAllListeners(theSource);
   }
   
   /**
    * T 17 test remove empty listeners.
    */
   @Test
   @Order(18)
   public void T17_testRemoveEmptyListeners()
   {
      Messenger.removeAllListeners("BadIdentifier");
   }
   
   /**
    * T 18 test send global message to no listener.
    */
   @Test
   @Order(19)
   public void T18_testSendGlobalMessageToNoListener()
   {
      Messenger.sendMessage(theSource, null, TheListener.FIRSTID, 666, false);
   }
}
