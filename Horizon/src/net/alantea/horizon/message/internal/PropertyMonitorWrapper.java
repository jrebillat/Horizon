package net.alantea.horizon.message.internal;

import net.alantea.horizon.message.Message;
import net.alantea.liteprops.Property;

/**
 * The Class PropertyMonitorWrapper.
 */
public class PropertyMonitorWrapper
{
   
   /** The property. */
   @SuppressWarnings("rawtypes")
   private Property property;
   
   /**
    * Instantiates a new property monitor wrapper.
    *
    * @param property the property
    */
   public PropertyMonitorWrapper(Property<?> property)
   {
      this.property = property;
   }
   
   /**
    * On message.
    *
    * @param message the message
    */
   @SuppressWarnings({ "unused", "unchecked" })
   private void onMessage(Message message)
   {
      Object destination = property.get();
      if ((destination != null) && (destination.getClass().isAssignableFrom(message.getContent().getClass())))
      {
         property.set(message.getContent());
      }
   }

}
