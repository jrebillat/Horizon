package net.alantea.horizon.message;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD) // TODO: Auto-generated Javadoc

 /**
  * The Interface Receives.
  */
 //can use in method only.
public @interface Receives 
{
   
   /**
    * Value.
    *
    * @return the receive[]
    */
   public Receive[] value();
}
